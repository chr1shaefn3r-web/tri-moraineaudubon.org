module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		useminPrepare: {
			html: 'build/index.html'
		},

		usemin: {
			html: ['build/index.html']
		},

		uncss: {
			build: {
				files: {
					'build/css/audubon.un.css': ['index.html', 'about_us.html', 'join_us.html']
				}
			}
		},

		cssmin: {
			build: {
				files: {
					'build/css/audubon.min.css': ['build/css/audubon.un.css']
				}
			}
		},

		critical: {
			build: {
				options: {
					base: 'build/',
					width: 260,
					height: 640
				},
				src: 'build/index.html',
				dest: 'build/index.html'
			}
		},

		htmlmin: {
			build: {
				options: {
					removeComments: true,
					removeCommentsFromCDATA: true,
					removeCDATASectionsFromCDATA: true,
					collapseWhitespace: true,
					removeAttributeQuotes: false
				},
				files: {
					'build/index.html': 'build/index.html'
				}
			}
		}
	});

	// Load all files starting with `grunt-`
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// Tell Grunt what to do when we type "grunt" into the terminal
	grunt.registerTask('default', ['optimize']);
	grunt.registerTask('optimize', ['usemin-optimize', 'css-mangling', 'htmlmin:build']);
	grunt.registerTask('usemin-optimize', ['useminPrepare', 'concat:generated', 'uglify:generated', 'usemin']);
	grunt.registerTask('css-mangling', ['uncss:build', 'cssmin:build', 'critical:build']);
};

