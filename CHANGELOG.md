# Changelog

## 2016-09-22 12:05

 * Changed Index.html to include the links for Environmental Educationa and Habitat Conservation within the slider.

## 2016-09-22 13:30

 * Added link to latest newsletter and added known events to calendar.

## 2016-09-23 14:20
 * Added newsletter archive.

