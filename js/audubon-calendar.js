$(document).ready(function() {
    $('#calendar').fullCalendar({
		events: [{
			title: "Tom Hisong: Nature Photography",
			start: "2016-09-06",
url: "./images/2016-09-06.png"
		},
{
			title: "John Mueller: Exotic Species",
			start: "2016-10-04"
		},
{
			title: "Fall Color Hike",
			start: "2016-10-15",
url: "./images/2016-10-15.png"
		},
{
			title: "Green Fire: Aldo Leopold (movie)",
			start: "2016-11-01",
url: "./images/2016-11-01.png"
		},
{
			title: "Fall Birdseed Sale",
			start: "2016-11-05"
		},
{
			title: "Potluck & Photo Safari",
			start: "2017-01-03"
		},
{
			title: "Craig Barr: Stories from a Wildlife Officer",
			start: "2017-02-07"
		},
{
			title: "Anne Smedley: Breeding Birds of Lippincott Bird Sanctuary",
			start: "2017-03-07"
		},
{
			title: "Bob Klips: Making Sense of Flowers",
			start: "2017-04-04"
		},
{
			title: " Leslie Riley: Aquatic Wildlife Near You!",
			start: "2017-05-02"
		},
]
    });
});

